# HTML TEMPLATE SENSITIVE

Can I ask you a question before starting?

Been to Bootstrap? Perhaps you have heard of it or, even better, you already know its strengths and weaknesses.
In any case, Bootstrap is a framework for creating websites and web applications , based on a large collection of HTML, CSS and Javascript resources and tools.
It can be used in conjunction with popular CMSs such as WordPress and Joomla by creating top-notch professional templates.
A good bootstrap template can make a real difference to your website or that of your customers.


DEMO
[https://www.blazetech.gradiscake.com/filescorporative/demoapp/html_template_sensitive/index.html](https://www.blazetech.gradiscake.com/filescorporative/demoapp/html_template_sensitive/index.html)


## Screenshot responsive design
[![Template sensitive responsive design](https://danzisi.bitbucket.io/html_template_sensitive/assets/sensitive_responsivedesign.jpg "Template sensitive responsive design")](https://danzisi.bitbucket.io/html_template_sensitive/assets/sensitive_responsivedesign.jpg "Template sensitive responsive design")


To get started, clone repo

## Contributing

> To get started...

### Step 1

- **Option 1**
    - Fork this repo!

- **Option 2**
    - Clone this repo to your local machine

### Step 2

- **HACK AWAY!**

### Step 3

- Create a new pull request

---

## Screenshot desktop
[![Template sensitive desktop](https://danzisi.bitbucket.io/html_template_sensitive/assets/sensitive_desktop.jpg "Template sensitive desktop")](https://danzisi.bitbucket.io/html_template_sensitive/assets/sensitive_desktop.jpg "Template sensitive desktop")


## Copyright and license

Copyright 2020.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this work except in compliance with the License.
You may obtain a copy of the License in the LICENSE file, or at:

  [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

